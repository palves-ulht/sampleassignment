package pt.ulusofona.deisi.dropProject.sampleAssignment;

import org.junit.*;
import static org.junit.Assert.assertEquals;
import java.security.*;

// all test classes must begin with "Test"
public class TestProject {

    @BeforeClass
    public static void setup() {
    }

    @AfterClass
    public static void teardown() {
    }

    @Test
    public void testFuncaoParaTestar() {
        assertEquals(3, Main.funcaoParaTestar());
    }

    @Test
    public void testFuncaoLentaParaTestar() {
        assertEquals(3, Main.funcaoLentaParaTestar());
    }
}